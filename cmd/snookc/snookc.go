package main

import (
	"os"
	"snook"
)

func main() {
	var addr string
	if len(os.Args) == 1 {
		addr = "127.0.0.1:1234"
	} else {
		addr = os.Args[1]
	}

	ui := snook.NewNcursesUi(addr)
	defer ui.Destroy()

	cmds := make(chan int)
	go snook.Transmitter(cmds, addr)
	for {
		cmds <- ui.ReadInput()
	}
}
