package snook

import (
	"fmt"
	"net"
	"os"
	"time"
)

type PlayerConn struct {
	cmds     chan int
	gameover chan bool
}

func newPlayerConn() PlayerConn {
	m := PlayerConn{}
	m.cmds = make(chan int)
	m.gameover = make(chan bool, 1)
	return m
}

func handler(conn net.Conn, mid PlayerConn) {
	defer conn.Close()
	defer close(mid.cmds)

	buf := make([]byte, 1)
	heading := 0
	timeout := 5 * time.Millisecond

	for {
		select {
		case <-mid.gameover:
			//fmt.Println("GAME OVER")
			return
		case mid.cmds <- heading:
		default:
			conn.SetDeadline(time.Now().Add(timeout))
			n_read, err := conn.Read(buf)
			if err != nil {
				neterr, ok := err.(net.Error)
				if !ok || !neterr.Timeout() {
					//fmt.Println("TCP read error:", err)
					return
				}
			} else if n_read > 0 {
				heading = int(buf[n_read-1]) % 4
			}
		}
	}
}

func NetDispatcher(pipe chan<- PlayerConn, addr string) {

	ln, err := net.Listen("tcp", addr)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	for { // Spawn handler and something to take on data
		conn, err := ln.Accept()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		mid := newPlayerConn()
		go handler(conn, mid)
		pipe <- mid

		time.Sleep(100 * time.Millisecond)
	}
}

func Transmitter(commands <-chan int, addr string) {

	conn, err := net.Dial("tcp", addr)
	if err != nil {
		println("Connection refused.")
		return
	}
	defer conn.Close()

	buf := make([]byte, 1)
	for i := range commands {
		buf[0] = byte(i)
		_, err = conn.Write(buf)
		if err != nil {
			panic(err)
		}
	}
}

func Echo(pipe <-chan PlayerConn) {
	for mid := range pipe {
		fmt.Println("New mid!")
		for c := range mid.cmds {
			fmt.Println(c)
			time.Sleep(250 * time.Millisecond)
		}
	}
}
