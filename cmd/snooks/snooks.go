package main

import (
	"os"
	"snook"
)

func main() {
	var addr string
	if len(os.Args) == 1 {
		addr = "127.0.0.1:1234"
	} else {
		addr = os.Args[1]
	}

	ui := snook.NewNcursesUi(addr)
	defer ui.Destroy()

	// Handle connecting players.
	pipe := make(chan snook.PlayerConn)
	go snook.NetDispatcher(pipe, addr)

	// Activate game.
	view := make(chan [][]int)
	rows, cols := ui.Size()
	go snook.RunGame(pipe, rows, cols, view)

	// Draw the views sent out by game.
	for board := range view {
		ui.Draw(board)
	}
}
