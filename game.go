package snook

import (
	"container/list"
	"math/rand"
	"time"
)

const START_LEN int = 5

var next_id = 1

type point struct {
	x int
	y int
}

type snook struct {
	heading int
	points  int
	growths int
	body    *list.List
	conn    PlayerConn
}

func newSnook(start point, conn PlayerConn) *snook {
	s := snook{rand.Intn(4), 1, START_LEN - 1, list.New(), conn}
	s.body.PushFront(start)
	return &s
}

func (s *snook) moveHead(rows, cols int) {
	heading, ok := <-s.conn.cmds
	if !ok {
		return // Connection lost.
	}
	diff := heading - s.heading
	if diff == 2 || diff == -2 {
		heading = s.heading
	}

	head := s.body.Front().Value.(point)
	switch heading {
	case 0:
		head.x++
	case 1:
		head.y++
	case 2:
		head.x--
	case 3:
		head.y--
	}
	if 0 <= head.x && head.x < rows && 0 <= head.y && head.y < cols {
		s.heading = heading
		s.body.PushFront(head)
	}
}

func (s *snook) moveTail() {
	if s.growths > 0 {
		s.growths--
	} else {
		back := s.body.Back()
		if back != nil {
			s.body.Remove(back)
		}
	}
}

func (s *snook) postMove(id int, collides [][]int) {
	if s.body.Len() > 0 {
		e := s.body.Front()
		p := e.Value.(point)
		if collides[p.x][p.y] != id {
			s.body.Remove(e)
		}
	}
}

type game struct {
	rows       int
	cols       int
	players    map[int]*snook // *snook to allow side effects
	appleExist bool
	apple      point
}

func (g *game) update() {
	for i, p := range g.players {
		if p.body.Len() == 0 {
			p.conn.gameover <- true
			delete(g.players, i)
		}
	}

	for _, p := range g.players {
		p.moveHead(g.rows, g.cols)
		p.moveTail()

		first := p.body.Front()
		if first != nil {
			head := first.Value.(point)
			if head.x == g.apple.x && head.y == g.apple.y {
				p.growths++
				g.appleExist = false
			}
		}
	}

	// Check if anyone moved onto someone else
	collisions := g.draw()
	for id, p := range g.players {
		p.postMove(id, collisions)
	}

	if !g.appleExist {
		g.apple = point{rand.Intn(g.rows), rand.Intn(g.cols)}
		g.appleExist = true
	}
}

func (g *game) addPlayer(conn PlayerConn) {
	start := point{rand.Intn(g.rows), rand.Intn(g.cols)}
	g.players[next_id] = newSnook(start, conn)
	next_id++
}

func (g *game) draw() [][]int {
	board := make([][]int, g.rows)
	for i := range board {
		board[i] = make([]int, g.cols)
	}

	for i, p := range g.players {
		for e := p.body.Front(); e != nil; e = e.Next() {
			p := e.Value.(point)
			if board[p.x][p.y] != 0 {
				board[p.x][p.y] = -1 // Already taken!
			} else {
				board[p.x][p.y] = i
			}
		}
	}
	if g.appleExist {
		board[g.apple.x][g.apple.y] = -2
	}
	return board
}

func RunGame(pipe <-chan PlayerConn, rows, cols int, view chan<- [][]int) {
	rand.Seed(time.Now().UTC().UnixNano())

	g := game{}
	g.rows = rows
	g.cols = cols
	g.players = make(map[int]*snook)
	g.apple = point{rand.Intn(g.rows), rand.Intn(g.cols)}
	g.appleExist = true

	for {
		// TODO: Goroutine this?
		select {
		case conn := <-pipe:
			g.addPlayer(conn)
		default:
			g.update()
			view <- g.draw()
			time.Sleep(150 * time.Millisecond)
		}
	}
}
