package snook

import (
	ncurses "github.com/rthornton128/goncurses"
)

type Ui interface {
	Draw([][]int)
	ReadInput() int
	Size() (int, int)
}
// TODO: Implement with gocui -> cross compile-able?

type NcursesUi struct {
	window *ncurses.Window
	title string
}

func NewNcursesUi(host string) NcursesUi {
	// Ncurses setup
	stdscr, err := ncurses.Init()
	if err != nil {
		panic(err)
	}

	ncurses.CBreak(true)
	ncurses.Echo(false)

	err = ncurses.Cursor(0)
	if err != nil {
		panic(err)
	}

	initColors()

	return NcursesUi{stdscr, host + " now accepting bizniz."}
}

func initColors() {
	err := ncurses.StartColor()
	if err != nil {
		panic(err)
	}

	var colors = []int16{
		//ncurses.C_WHITE,
		ncurses.C_BLUE,
		ncurses.C_CYAN,
		ncurses.C_GREEN,
		ncurses.C_MAGENTA,
		ncurses.C_RED,
		ncurses.C_YELLOW}

	for i, c := range colors {
		err := ncurses.InitPair(int16(i+1), c, ncurses.C_BLACK)
		if err != nil {
			panic(err)
		}
	}
}

func (ui NcursesUi) Destroy() {
	ui.window = nil
	ncurses.End()
}

func (ui NcursesUi) Size() (int, int) {
	rows, cols := ui.window.MaxYX()
	return rows-2, cols-2
}

func interpKey(char uint8) int {
	switch char {
	case 65:
		return 2
	case 66:
		return 0
	case 67:
		return 1
	//case 68:
	default:
		return 3
	}
}

func (ui NcursesUi) ReadInput() int {
	return interpKey(uint8(ui.window.GetChar()))
}

func (ui NcursesUi) Draw(board [][]int) {
	ui.window.Erase()
	err := ui.window.Box(0, 0)
	if err != nil {
		panic(err)
	}
	ui.window.Print(ui.title)

	for i := range board {
		for j := range board[i] {
			switch b := board[i][j]; b {
			case -2:
				ui.window.MoveAddChar(i+1, j+1, ncurses.ACS_DIAMOND|ncurses.ColorPair(3))
			case 0:
				// Blank.
			default:
				color := ncurses.ColorPair(int16(b % 6))
				ui.window.MoveAddChar(i+1, j+1, ncurses.ACS_BLOCK|color)
			}
		}
	}
	ui.window.Refresh()
}

func checker(window *ncurses.Window, shift int) {
	last_row, last_col := window.MaxYX()

	for row := 1; row < last_row-1; row++ {
		for col := 1; col < last_col-1; col++ {
			c := int16((row + col + shift) % 6)
			window.MoveAddChar(row, col,
				ncurses.ACS_BLOCK|ncurses.ColorPair(c))
		}
	}
	window.Refresh()
}
